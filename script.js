// Init variables
let qft_projectName = '';
let qft_taskType = '';
let qft_workHoursByDay = 0;
let qft_timeOffOnTheWeek = [];
let qft_workingDays = [];

document.addEventListener('onStorageChange', function (e) {
    var data = e.detail;
    refreshDataFromStorage(data);
});

function refreshDataFromStorage(data) {
    qft_projectName = data.projectName;
    qft_taskType = data.taskType;
    qft_workHoursByDay = data.workHoursByDay;
    qft_workingDays = data.workingDays;

    // Add Button
    $('.sectionTabsArea').css( 'display', 'flex' );
    $('.sectionTabsArea ul').css( 'width', '50%' );
    $('.quickFillContainer').remove();

    if (!qft_projectName || !qft_taskType) {
        $('.sectionTabsArea').append( '<div class="quickFillContainer"><div class="quickFillButton disabled">Quick Fill</div></div>');
        // Add Click Handlers
        $('.quickFillButton').click(function() { alert(`Please update QuickFill settings first.`); });
    } else {
        $('.sectionTabsArea').append( '<div class="quickFillContainer"><div class="quickFillButton">Quick Fill</div></div>');

        // Add Click Handlers
        $('.quickFillButton').click(onQuickFillButtonClicked);
    }
}

function onQuickFillButtonClicked() {
    if ($('tbody[sectiontype="rows"] tr:first-child td:first-child').length === 0) {
        addNewTimeline();
    } else {
        selectProjectName();
    }
}

function addNewTimeline() {
    $('#add-new-timeline')[0].click();
    setTimeout(selectProjectName, 1000);
}

function selectProjectName() {
    const $button = $('tbody[sectiontype="rows"] tr:first-child td:first-child a');
    waitDropdownLoadedAndClickOnId($button, qft_projectName, 'Project name', selectTaskType);
}

function selectTaskType() {
    const $button = $('tbody[sectiontype="rows"] tr:first-child td.activity a');
    waitDropdownLoadedAndClickOnId($button, qft_taskType, 'Task Type', calculateWeekWorkHoursRemaining);
}

function waitDropdownLoadedAndClickOnId($button, searchByName, type, callback, index = 0) {
    const dropDownContentId = $button.attr('dropdowncontentid');
    const $divDropdownContent = $('#' + dropDownContentId);
    if (index === 25) {
        return;
    }
    if (!$divDropdownContent.is(":visible")) {
        $button[0].click();
        setTimeout(() => waitDropdownLoadedAndClickOnId($button, searchByName, type, callback, index + 1), 300);
        return;
    }

    const $dropdownList = $(`#${dropDownContentId} ul.divDropdownList li a`);
    if ($dropdownList.length === 0) {
        setTimeout(() => waitDropdownLoadedAndClickOnId($button, searchByName, type, callback, index + 1), 200);
        return;
    }

    const $dropdownListElement = $(`#${dropDownContentId} ul.divDropdownList li a:contains("${searchByName}")`);
    if ($dropdownListElement.length === 0) {
        alert(`${type} (${searchByName}) not found!`);
        return;
    }
    $dropdownListElement[0].click();
    setTimeout(callback, 500);
}

function calculateWeekWorkHoursRemaining() {
    qft_timeOffOnTheWeek = Array(7).fill(0);
    $('tbody[section="1"][sectiontype="rows"] tr.timeOff td a').each(function(index) {
        const value = Number($(this).text());
        if(!isNaN(value)) {
            qft_timeOffOnTheWeek[index%7] += value;
        }
    });
    setTimeout(fillWeekWorkHours, 200);
}

function fillWeekWorkHours() {
    $('tbody[section="0"][sectiontype="rows"] tr:first-child td.day input').each(function(index) {
        $(this).focus();
        const remainingHours = Math.max(qft_workHoursByDay - qft_timeOffOnTheWeek[index], 0);
        const value = qft_workingDays[index] && remainingHours > 0 ? remainingHours : '';
        $(this).val(value);
        $(this).blur();
    });
}

