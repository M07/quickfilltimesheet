
var s = document.createElement('script');
s.src = chrome.runtime.getURL('script.js');
s.onload = function() {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);


function refreshDataFromStorage() {
    getFromStorage({ projectName: '', taskType: '', workHoursByDay: 7.5, workingDays:[false, false, true, true, true, true, true] }
    , function (data) {
        if (window.navigator.userAgent.includes('Chrome')) {
            document.dispatchEvent(new CustomEvent('onStorageChange', {
                detail: data,
            }));
        } else {
            document.dispatchEvent(new CustomEvent('onStorageChange', {
                detail: cloneInto(data, document.defaultView),
            }));
        }
    });
}

$( document ).ready(function() {
    // Get data from storage

    setTimeout(refreshDataFromStorage, 250);
    chrome.storage.onChanged.addListener(function() {
        refreshDataFromStorage();
    });
});


function getFromStorage(value, callback) {
    if (window.navigator.userAgent.includes('Chrome')) {
        return chrome.storage.sync.get(value, callback);
    } else  {
        return browser.storage.sync.get(value).then(callback);
    }
}
