// Saves options to chrome.storage
function save_options() {
    var projectName = document.getElementById('projectName').value;
    var taskType = document.getElementById('taskType').value;
    var workHoursByDay = document.getElementById('workHoursByDay').value;
    var workingDays = [
      document.getElementById('weekday-sat').checked,
      document.getElementById('weekday-sun').checked,
      document.getElementById('weekday-mon').checked,
      document.getElementById('weekday-tue').checked,
      document.getElementById('weekday-wed').checked,
      document.getElementById('weekday-thu').checked,
      document.getElementById('weekday-fri').checked
    ];

    setFromStorage({
        projectName,
        taskType,
        workHoursByDay,
        workingDays
    }, function() {
      // Update status to let user know options were saved.
      var status = document.getElementById('status');
      status.textContent = 'Options saved.';
      setTimeout(function() {
        status.textContent = '';
      }, 750);
    });
  }
  
  // Restores select box and checkbox state using the preferences
  // stored in chrome.storage.
  function restore_options() {
    // Use default value color = 'red' and likesColor = true.
    getFromStorage({
        projectName: '',
        taskType: '',
        workHoursByDay: 7.5,
        workingDays: [false, false, true, true, true, true, true]
    }, function(data) {
      document.getElementById('projectName').value = data.projectName;
      document.getElementById('taskType').value = data.taskType;
      document.getElementById('workHoursByDay').value = data.workHoursByDay;

      // workingDays
      document.getElementById('weekday-sat').checked = data.workingDays[0];
      document.getElementById('weekday-sun').checked = data.workingDays[1];
      document.getElementById('weekday-mon').checked = data.workingDays[2];
      document.getElementById('weekday-tue').checked = data.workingDays[3];
      document.getElementById('weekday-wed').checked = data.workingDays[4];
      document.getElementById('weekday-thu').checked = data.workingDays[5];
      document.getElementById('weekday-fri').checked = data.workingDays[6];      
    });
  }
  document.addEventListener('DOMContentLoaded', restore_options);
  document.getElementById('save').addEventListener('click', save_options);
  

function getFromStorage(value, callback) {
  if (window.navigator.userAgent.includes('Chrome')) {
    return chrome.storage.sync.get(value, callback);
  } else  {
    return browser.storage.sync.get(value).then(callback);
  }
}

function setFromStorage(value, callback) {
  if (window.navigator.userAgent.includes('Chrome')) {
    return chrome.storage.sync.set(value, callback);
  } else  {
    return browser.storage.sync.set(value).then(callback);
  }
}